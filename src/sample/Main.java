package sample;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class Main extends Application {

    Controller controller;
    AnimationTimer animationTimer;
    AppLoop appLoop;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void stop() throws Exception {
        System.out.print("Exiting...");
        super.stop();
        appLoop.stop();
        System.out.println("\t[OK]");
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("sample.fxml"));
        Parent root = fxmlLoader.load();
        this.controller = fxmlLoader.<Controller>getController();       // get the controller from fxml file
        primaryStage.setTitle("Goldmann Game");
        primaryStage.setResizable(false);
        primaryStage.setScene(new Scene(root));

        controller.printHello();    // you can reach controller methods
        System.out.println("Height of canvas " + controller.getBackGround().getHeight());
        System.out.println("Width of canvas " + controller.getBackGround().getWidth());

        primaryStage.show();    // show window

        // create the main loop for application logic
        appLoop = new AppLoop();
        appLoop.setController(controller);
        appLoop.start();

    }

}

