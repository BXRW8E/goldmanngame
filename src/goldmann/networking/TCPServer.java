package goldmann.networking;

import goldmann.tools.network.GetIPV4Address;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Scanner;

public class TCPServer extends Network {

    private int port;
    private Integer passwordCompare;

    public TCPServer(int port) {
//        super(onReceiveCallback);
        super();
        this.port = port;
        System.out.println("Creating server... ");
    }


    public TCPServer(int port, int password) {
//        super(onReceiveCallback);
        super(Integer.valueOf(password));
        this.port = port;
        System.out.println("Creating server... ");
    }


    @Override
    public boolean isServer() {
        return true;
    }

    @Override
    public String getIP() {
        return null;
    }

    @Override
    public int getPort() {
        return port;
    }

    /**
     * test function for the TCPServer class
     * @param args
     * @throws InterruptedException
     * @throws UnknownHostException
     */
    public static void main(String[] args) throws InterruptedException, UnknownHostException {

        TCPServer server = null;
        Thread createServer;
        Scanner keyboard;
        server = new TCPServer(4444);

        ArrayList<String> myIp = GetIPV4Address.getIpAddress();
        for (String ip : myIp) {
            System.out.println("My ip is: " + ip);
        }

        server.startConnection();
        server.passwordCompare = 1234;
        keyboard = new Scanner(System.in);
        while (true) {

//            if (server.passwordCompare.equals(server.getPassword())) {
//                System.out.println("password ok");
//            }
//
//            String msg = keyboard.nextLine();
//            try {
//                server.send(msg);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
        }


    }
}
