package goldmann.tools;

/**
 * Algorithm logic during the game
 */
public enum GameStates {
    STARTING,
    STARTED,
    INGAME,
    STARTINGAME,
    CONNECTING,
    CONNECTED,
    PAUSED,
    ENDED,
    TIMEISUP,
    BROKENCONNECTION,
    WRONGPASSWORD,
    WAITFORRESTART,
    RETURNINGAME,
    NONE
}
