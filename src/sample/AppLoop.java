package sample;

import javafx.animation.AnimationTimer;
import javafx.scene.image.Image;


public class AppLoop extends AnimationTimer {

    private Controller controller;
    private long lastTime;
    private long currentTime;
    private int state;
    private double x;
    private double y;
    private int up;
    private Image figure;
    private Image stepsLeft[];
    private Image stepsRight[];
    private int stepCounter;
    private int stepSize;

    public AppLoop() {
        super();
        //controller = new Controller();
        //controller.setAnimationTimer(this);
        currentTime = System.nanoTime();
        lastTime = currentTime;
        state = 0;
        x = 0;
        y = 200;
        up = 0;
        stepSize = 70;

        stepsLeft = new Image[4];
        stepsLeft[0] = new Image("file:./resources/character1/step1l.png");
        stepsLeft[1] = new Image("file:./resources/character1/step2l.png");
        stepsLeft[2] = new Image("file:./resources/character1/step3l.png");
        stepsLeft[3] = new Image("file:./resources/character1/step4l.png");

        stepsRight = new Image[4];
        stepsRight[0] = new Image("file:/home/mate/IdeaProjects/GoldmannGame/resources/character1/step1r.png");
        stepsRight[1] = new Image("file:/home/mate/IdeaProjects/GoldmannGame/resources/character1/step2r.png");
        stepsRight[2] = new Image("file:/home/mate/IdeaProjects/GoldmannGame/resources/character1/step3r.png");
        stepsRight[3] = new Image("file:/home/mate/IdeaProjects/GoldmannGame/resources/character1/step4r.png");
        stepCounter = 0;
        figure = stepsLeft[stepCounter];


    }

    @Override
    public void handle(long now) {

        if (currentTime - lastTime >= 3e8) {
            stepCounter = (stepCounter++) % 4;
          ///  System.out.println("Hello world");
           // System.out.println("x: " + x + " y: " + y);


//            controller.getGameContext().drawImage(figure,x,y);
            controller.drawFigure(controller.getBackgroundContext(),figure, x, y);
//            controller.drawFigure(figure, x, y);
            if (this.up == 0) {
                this.x += stepSize;
                figure = stepsLeft[stepCounter];
                if (this.x >= 900)
                    this.up = 1;
            } else if (this.up == 1) {
                this.x -= stepSize;
                figure = stepsRight[stepCounter];
                if (this.x <= 20)
                    this.up = 0;
            }
            lastTime = currentTime;

        }
        currentTime = System.nanoTime();

    }

    public void setController(Controller controller) {
        this.controller = controller;
    }
}
