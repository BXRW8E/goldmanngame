# README #

# Goldmann Game

## General
- Két játékos egymás ellen harcol
- játék elején választható a karakter, eszköz, (helyszín)
- Játékos megadhatja a nevét
- ne kelljen új játékhoz újra kapcolódni --> visszavágó indítása
 (3 életig mehet)
 (néha leesik egy szív)
- szerver-kliens, szerver IP címét megadja a kliens emberke
	- (ez továbbfejleszthető úgy, hogy Listboxban az elérhető ellenfelek és abból tudsz választani)
	- a játék elején megadod hogy szerver vagy kliens vagy (újat akarsz létrehozni vagy csatlakozni, mint honfoglalóban a különszoba)

*****************************************
 <p>Indul a játék</p> 

* alapból egyszintes a játéktér, és a játékablakkal megegyező nagyságú
* szerepeljen a felső sorban a játékosok neve, élete, középen visszaszámláló, pause gomb, feladás gomb
* játékosok nem összeütköznek hanem elmennek egymás mellett
* amerre megy éppen mindig arra fordul
* pillanatnyi gombnyomásra csak fordul, folyamatos gombnyomásra megy
* tud olyan magasra ugrani ahol nem éri el az ütés
* átfedésben ne lehessenek a játékosok teljesen
* legyen az ütés pillanatszerű
	* eszközöknél eltérő lehet az ütés maximális frekvenciája
	* eszközök hatósugara megegyező

*************************************************************************************************
# Leírás

Játékunk a lövöldözős játék kategóriába esik, de annak egy speciális alfaját valósítja meg, ahol távoli harc helyett, közelharcban kell megküzdeniük egymással a játékosoknak. Játék két karakter egymás közötti harcáról szól, melynek célja, hogy egyik fél legyőzze a másikat. A játékosok egy-egy saját maguk által választott karakterrel vesznek részt a küzdelemben. Meccs elején kiválasztható a játékos neve, karaktere. A játék lefolyása online zajlik (honfoglaló külön szobákhoz hasonlóan), ahol a kihívást létrehozó félhez (szerver) csatlakozik a kihívást elfogadó játékos (kliens). Az online játék TCP/IP kapcsolaton keresztül zajlik, ahol a szerver játékos IP címének ismertnek kell lennie a kliens számára, hogy csatlakozni tudjon hozzá. A szerver IP címét a játékablak megjeleníti, illetve ad egy hozzá tartozó négy jegyű számot, amely jelszóként funkcionál
a kliens számára, hogy csak a kívánt játékos tudjon csatlakozni a szerverhez. A szerverhez egy időben egy kliens csatlakozhat. 

A kapcsolat sikeres kiépítése után a játékosok egy közös csatatér bal és jobb oldalán, véletlenszerűen jelennek meg. Ezt követően bal és jobb irányba tudnak mozogni a karakterek, valamint ugrásra és ütésre is lehetőség van.A játék során ütésre bármikor van lehetőség, azonban csak akkor számít sebzésnek, ha az ellenséges játékos a támadó eszköz hatósugarán belül van. A játék addig tart, amíg az egyik játékosnak el nem fogy az élete. A játék menete során lehetőség van "feladás" opcióra is. A meccs végén lehetőség van új játékot indítani, vagy bontani a kapcsolatot. A játék abban az esetben indul újra, ha mind a két résztvevő egyaránt beleegyezik az új játékba.

A játék megjelenítése 2 dimenziós oldalnézetű, minimális retro játék szerű grafikát használ. A képernyő közepén van egy visszaszámláló óra, amely a játék időkorlátjáért felelős. A játék több pályát is szolgáltat, amelyek a háttérképben különböznek. 

# Opcionális dolgok:
* késelés
* háttérzene
* ütéshang

# Felhasznált technológiák:
* Java 9
* JavaFx
