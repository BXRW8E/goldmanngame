package goldmann.networking;

import goldmann.GoldmannGame;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.function.Consumer;

/**
 * Created by mate on 2016.11.19..
 * Abstarct class. This is for the network communication. All object can be send wich implements Seriaziable interface
 */
public abstract class Network {

    //    private Consumer<Serializable> onReceiveCallback;
    private Consumer<String> onReceiveCallback;
    final private ConnectionThread connectionThread = new ConnectionThread();
    private Integer password;
    protected String ipAddress;
    private Consumer<String> pwdCallback;
    private NetworkStates state;


    /**
     * Create a NEtwork object, set the status to connecting
     */
    public Network() {
        connectionThread.setDaemon(true);
//        pwdCallback = this::receivePassword;
        state = NetworkStates.STARTING;
    }

    public Network(Integer password) {
        connectionThread.setDaemon(true);
        this.password = password;
        pwdCallback = this::receivePassword;
        state = NetworkStates.STARTING;
    }

    /**
     * Constructor
     *
     * @param onReceiveCallback
     */
    public Network(Consumer<Serializable> onReceiveCallback) {
//        this.onReceiveCallback = onReceiveCallback;
        connectionThread.setDaemon(true);
    }

    public Integer getPassword() {
        return password;
    }

    public void setPassword(Integer password) {
        this.password = password;
    }

    public void receivePassword(String msg) {
        System.out.println("Password received");
        this.password = Integer.getInteger(msg);
    }

    public NetworkStates getState() {
        return this.state;
    }

    public void setState(NetworkStates state) {
        this.state = state;
    }

    /**
     * Starts a thread for the communication and for the connection
     */
    public void startConnection() {
        state = NetworkStates.WAITING;
        connectionThread.shouldRun = true;
        connectionThread.start();
    }

    /**
     * Closes the socket and stops the thread
     *
     * @throws IOException
     */
    public void closeConnection() throws IOException {
        connectionThread.shouldRun = false;
        if (state == NetworkStates.CONNECTED) {
            connectionThread.out.close();
            connectionThread.in.close();
            connectionThread.socket.close();
            if (isServer()) {
                if (connectionThread.server != null) {
                    connectionThread.server.close();
                }
            }

        } else if (state == NetworkStates.WAITING || state == NetworkStates.STARTING) {
            if (isServer()) {
//                connectionThread.server.close();
            }
        }
        state = NetworkStates.DISCONNECTED;

        if (connectionThread.in != null) {
            connectionThread.in.close();
        }
        if (connectionThread.out != null) {
            connectionThread.out.close();
        }
        if (connectionThread.server != null) {
            if (!connectionThread.server.isClosed()) {
                connectionThread.server.close();
            }
        }
    }

    public boolean isNewMessage() {
        return connectionThread.isNewMessage;
    }

    public void setIsNewMessage(boolean read) {
        connectionThread.isNewMessage = read;
    }

    /**
     * Every type of datas can be send, just it must impelent the Serializable interface
     *
     * @param data
     * @throws IOException
     */
    public void send(Serializable data) throws IOException {
        //if(connectionThread.out != null) {
        connectionThread.out.writeObject(data);
        //}
    }

    public String getData() {
        return connectionThread.data;
    }

    public abstract boolean isServer();

    protected abstract String getIP();

    protected abstract int getPort();

//    protected Serializable receive(){
//        String msg
//        return msg;
//    }


    private class ConnectionThread extends Thread {
        private Socket socket;
        private ServerSocket server;
        private ObjectOutputStream out;
        private ObjectInputStream in;
        private boolean isFirst;
        int cntr;
        private boolean isNewMessage;
        private String data;
        private boolean shouldRun;
        private boolean isOpened = false;

        /**
         * in this thread is the communication. if the connection was succesfully, sets
         * the status to CONNECTED
         */
        @Override
        public void run() {
            try (ServerSocket server = isServer() ? new ServerSocket(getPort()) : null;
                 Socket socket = isServer() ? server.accept() : new Socket(getIP(), getPort());
                 ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
                 ObjectInputStream in = new ObjectInputStream(socket.getInputStream())) {

                System.out.println("Starting...");
                setState(NetworkStates.CONNECTED);

                cntr = 0;
                //onReceiveCallback = MyConsumer::printmsg;       /* Consumer. It is an IT handler */
//                onReceiveCallback = MyConsumer::receiveGGMessage;
                onReceiveCallback = GoldmannGame::receiveGGMessage;
                isFirst = true;
                isNewMessage = false;
                this.server = server;
                this.socket = socket;
                this.in = in;
                this.out = out;

                socket.setTcpNoDelay(true);   // faster sending

                while (shouldRun == true) {
//                    if (isFirst && isServer()) {
//                        data = (String) in.readObject();       /* receive password callback */
//                        pwdCallback.accept(data);
//                        isFirst = false;
//                    }

                    data = (String) in.readObject();
                    onReceiveCallback.accept(data);
                    // System.out.println(data);
                }


            } catch (IOException e) {
//                onReceiveCallback.accept("Connection closed");

            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }


    } //connectionthread


} //networkconnection
