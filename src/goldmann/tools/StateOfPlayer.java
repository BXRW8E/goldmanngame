package goldmann.tools;


public enum StateOfPlayer {
    STEP_L(0),
    STEP_R(1),
    STANDING_L(2),
    STANDING_R(3),
    PUNCH_L(4),
    PUNCH_R(5),
    PUNCH_STEP_L(6),
    PUNCH_STEP_R(7),
    LOSE_L(9),
    WIN_L(10);

    private final int stateID;

    StateOfPlayer(int stateID) {   // constructor
        this.stateID = stateID;
    }

    public int getStateID() {
        return stateID;
    }
}