package goldmann;


import goldmann.GUI.AppLoop;
import goldmann.algorithm.LongValue;
import goldmann.networking.Network;
import goldmann.tools.json.GGMEssage;
import goldmann.tools.json.JsonParserGG;
import javafx.application.Application;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;

public class GoldmannGame extends Application {
    private static AppLoop appLoop;
    private Network network;
    private static ArrayList<GGMEssage> ggmEssages;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void stop() throws Exception {
        System.out.print("Exiting...");

        super.stop();
        appLoop.stop();
        System.out.println("\t[OK]");

    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        final long startNanoTime = System.nanoTime();


        // gameMenu(primaryStage, startNanoTime);
        LongValue lastNanoTime = new LongValue(System.nanoTime());
        appLoop = new AppLoop(startNanoTime, lastNanoTime, primaryStage);
        appLoop.start();

    }

    public static void receiveGGMessage(String jsonString) {
        GGMEssage ggmEssage = JsonParserGG.toGGMessage(jsonString);
        //    System.out.println(ggmEssage);
        appLoop.setGgmEssages(ggmEssage);
    }


}
