package goldmann.tools.json;


import com.google.gson.Gson;

import java.util.ArrayList;

public class JsonParserGG {

    private String message;
    private ArrayList<Object> fileds;
    private Gson json;

    public JsonParserGG() {
        fileds = new ArrayList<Object>();
        json = new Gson();
    }

    /**
     * adds data to the list
     *
     * @param data
     */
    public void addData(Object data) {
        fileds.add(data);
    }

    /**
     * flushes all of data to the screen
     */
    public void flush() {
        json.toJson(fileds, System.out);
    }

    public String toJson() {
        return json.toJson(fileds);
    }

    /**
     * parses every Object to json String
     * @param data every type wich is inherited from Object class
     * @return  jsonString represented from data
     */
    public static String toJson(Object data) {
        Gson json = new Gson();
        return json.toJson(data);
    }

    /**
     * parses the input String to a GGmessage object
     *
     * @param jsonString is the input String in Json format
     * @return GGMessage object from the input string
     */
    public static GGMEssage toGGMessage(String jsonString) {
        GGMEssage data;
        Gson gson = new Gson();

        data = gson.fromJson(jsonString, GGMEssage.class);
        return data;
    }


    /**
     * basic example
     *
     * @param args
     */
    public static void main(String[] args) {
        int pos[] = {100, 200};
        String codes[] = {"UP", "DOWN", "LEFT", "RIGHT", "", "", "", ""};
        //  System.out.println(codes.length);
        GGMEssage test = new GGMEssage();
        JsonParserGG jsonParser = new JsonParserGG();
        int password = 333444;

        String jsonString = JsonParserGG.toJson(test);  //convert test object to json
        Gson gson = new Gson();

        GGMEssage rec = JsonParserGG.toGGMessage(jsonString);

        System.out.println(test);
        System.out.println();
        System.out.println();
        System.out.println(jsonString);
        System.out.println();
        System.out.println(rec);
    }

}

