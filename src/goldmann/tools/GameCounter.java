package goldmann.tools;

import java.util.Timer;
import java.util.TimerTask;

public class GameCounter {
    private Timer timer;
    private int currenttime;
    private String time;
    public GameCounter(int seconds) {
        timer = new Timer();
        time = "";
        currenttime = seconds;
        timer.schedule(new RemindTask(), seconds*1000);
        timer.scheduleAtFixedRate(new TickTask(), 0,1000);
    }

    public String getTime() {
        return time;
    }

    public void cancel(){
        this.timer.cancel();
    }
    class TickTask extends TimerTask{
        public void run() {
            time = String.format("%02d", currenttime/60)+":"+String.format("%02d", currenttime%60);
            currenttime -= 1;
        }
    }
    class RemindTask extends TimerTask {
        public void run() {
            System.out.println("Game over");
            timer.cancel(); //A timer szál megszüntetése
        }
    }
}
