# Goldmann Game

## Leírás
<style>
p {
    text-indent: 50px;
    text-align: justify;
    font-size: 80%;
}
.list{
    font-size: 80%;
}
</style>

<p>Játékunk a lövöldözős játék kategóriába esik, de annak egy speciális alfaját valósítja meg, ahol távoli harc helyett, közelharcban kell megküzdeniük egymással a játékosoknak. Játék két karakter egymás közötti harcáról szól, melynek célja, hogy egyik fél legyőzze a másikat. A játékosok egy-egy saját maguk által választott karakterrel vesznek részt a küzdelemben. Meccs elején kiválasztható a játékos neve, karaktere. A játék lefolyása online zajlik honfoglaló külön szobákhoz hasonlóan. 
</p>
<p>
A kapcsolat sikeres kiépítése után a játékosok egy közös csatatérre kerülnek. Ezt követően bal és jobb irányba tudnak mozogni a karakterek, valamint ugrásra és ütésre is lehetőség van. A játékosok sebzést csak kézzel tudnak bevinni. A játék addig tart, amíg az egyik játékosnak el nem fogy az élete, vagy le nem telik az idő. A játék menete során lehetőség van "feladás" opcióra is. A játék végén lehetőség van visszavágó kérésére, ehhez mindkét játékos közös beleegyezése szükséges.
</p>
<p>
A játék megjelenítése 2 dimenziós oldalnézetű, minimális retro játék szerű grafikát használ. A képernyő közepén van egy visszaszámláló óra, amely a játék időkorlátjáért felelős. A játék több pályát is szolgáltat, amelyek a háttérképben különböznek. 
<p>

## Játékszabályok 

<p>
Játék célja, hogy az ellenfél élete előbb fogyjon el mint a sajátunk. Az egyik játékos halálával a küzdelem véget ér. Csak az ütések megengedettek. Ezen kívül további megkötés nincsen.
</p>



## Opcionális dolgok:
* <div class="list">késelés</div>
* <div class="list">háttérzene</div>
* <div class="list">ütéshang</div>

## Csapattagok:
* <div class="list">Bártfai Máté</div>
* <div class="list">Beke Gábor</div>
* <div class="list">H. Zováthi Örkény</div>

</div>