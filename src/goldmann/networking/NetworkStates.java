package goldmann.networking;

public enum NetworkStates {
        STARTING,
        WAITING,
        CONNECTED,
        DISCONNECTED,
}
