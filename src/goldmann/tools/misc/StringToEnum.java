package goldmann.tools.misc;

/**
 * @author mate
 */
public class StringToEnum {

    /**
     * Converts a string to specific enum types
     * @param c is the enum class type
     * @param string is the String wich must be converted to enum
     * @param <T>   enum type
     * @return the enum type from String
     */
    public static <T extends Enum<T>> T getEnumFromString(Class<T> c, String string) {
        if (c != null && string != null) {
            try {
                return Enum.valueOf(c, string.trim().toUpperCase());
            } catch (IllegalArgumentException ex) {
            }
        }
        return null;
    }
}
