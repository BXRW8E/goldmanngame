package goldmann.networking;

import goldmann.GUI.Position;
import goldmann.tools.GameStates;
import goldmann.tools.json.GGMEssage;
import goldmann.tools.json.JsonParserGG;
import goldmann.tools.network.GetIPV4Address;

import java.io.IOException;

public class TCPClient extends Network {

    private int port;


    public TCPClient(String ipAddress, int port) {
        super();
        this.port = port;
        this.ipAddress = ipAddress;
        System.out.println("Creating client...");
    }

    @Override
    public boolean isServer() {
        return false;
    }

    @Override
    public String getIP() {
        return this.ipAddress;
    }

    @Override
    public int getPort() {
        return port;
    }

    /**
     * Test function for the TCPCLient class
     * @param args innput arguments
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        System.out.println("IP is: " + GetIPV4Address.getIpAddress());
        TCPClient client = new TCPClient((GetIPV4Address.getIpAddress().get(0)), 4444);
        client.startConnection();
        int password = 1234;
        String name = "client";
        int pos[] = {1, 2};
        String charType = "RED";
        String time = "02:45";
        String codes[] = {"", "", "UP", "DOWN", "LEFT", "SPACE", "RIGHT", ""};
        GameStates state = GameStates.CONNECTED;
        Position pl1 = new Position(10, 20, false);
        Position pl2 = new Position(40, 50, true);

        GGMEssage msg = new GGMEssage();

        for (int i = 0; i < 5; i++) {
            client.send(JsonParserGG.toJson(msg));
        }

//        Scanner keyboard = new Scanner(System.in);
        while (true) {
//            String msg = keyboard.nextLine();
//            try {
//                client.send(msg);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
        }

    }
}
