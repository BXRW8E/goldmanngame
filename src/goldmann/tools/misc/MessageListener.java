package goldmann.tools.misc;

public interface MessageListener {
    public void onMessage(String message);
}
