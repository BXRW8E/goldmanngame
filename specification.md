# Goldmann Game

## Leírás
<style>
p {
    text-indent: 50px;
    text-align: justify;
    font-size: 80%;
}
.list{
    font-size: 80%;
}
</style>

<p>Játékunk a lövöldözős játék kategóriába esik, de annak egy speciális alfaját valósítja meg, ahol távoli harc helyett, közelharcban kell megküzdeniük egymással a játékosoknak. Játék két karakter egymás közötti harcáról szól, melynek célja, hogy egyik fél legyőzze a másikat. A játékosok egy-egy saját maguk által választott karakterrel vesznek részt a küzdelemben. Meccs elején kiválasztható a játékos neve, karaktere. A játék lefolyása online zajlik (honfoglaló külön szobákhoz hasonlóan), ahol a kihívást létrehozó félhez (szerver) csatlakozik a kihívást elfogadó játékos (kliens). Az online játék TCP/IP kapcsolaton keresztül zajlik, ahol a szerver játékos IP címének ismertnek kell lennie a kliens számára, hogy csatlakozni tudjon hozzá. A szerver IP címét a játékablak megjeleníti, illetve ad egy hozzá tartozó négy jegyű számot, amely jelszóként funkcionál
a kliens számára, hogy csak a kívánt játékos tudjon csatlakozni a szerverhez. A szerverhez egy időben egy kliens csatlakozhat. 
</p>
<p>
A kapcsolat sikeres kiépítése után a játékosok egy közös csatatér bal és jobb oldalán, véletlenszerűen jelennek meg. Ezt követően bal és jobb irányba tudnak mozogni a karakterek, valamint ugrásra és ütésre is lehetőség van. A játék során ütésre bármikor van lehetőség, azonban csak akkor számít sebzésnek, ha az ellenséges játékos a támadó eszköz hatósugarán belül van. A játék addig tart, amíg az egyik játékosnak el nem fogy az élete. A játék menete során lehetőség van "feladás" opcióra is. A meccs végén lehetőség van új játékot indítani, vagy bontani a kapcsolatot. A játék abban az esetben indul újra, ha mind a két résztvevő egyaránt beleegyezik az új játékba.
</p>
<p>
A játék megjelenítése 2 dimenziós oldalnézetű, minimális retro játék szerű grafikát használ. A képernyő közepén van egy visszaszámláló óra, amely a játék időkorlátjáért felelős. A játék több pályát is szolgáltat, amelyek a háttérképben különböznek. 
<p>

<p>
A játékban lehet ütni, ugrani, jobbra-balra mozogni
</p>

<br/>
<br/>
<br/>
<br/>

## Opcionális dolgok:
* <div class="list">késelés</div>
* <div class="list">háttérzene</div>
* <div class="list">ütéshang</div>

## Felhasznált technológiák:
* <div class="list">Java 9</div>
* <div class="list">JavaFx</div>

</div>