package goldmann.tools;

public class Jump_Tool {
    private boolean jump_enabled;
    private double timeup;

    /**
     * Helps the characters to jump continuosly
     * @param jump_enabled: if a jump is alreagy began
     * @param time: the duration of the jump move
     */
    public Jump_Tool(boolean jump_enabled, double time) {
        this.jump_enabled = jump_enabled;
        this.timeup = time;
    }

    public boolean isJump_enabled() {
        return jump_enabled;
    }

    public void setTimeup(double time) {
        this.timeup = time;
    }

    public void setJump_enabled(boolean jump_enabled) {
        this.jump_enabled = jump_enabled;
    }

    public boolean isJumpUp(double t) {
        if (t < timeup) {
            return true;
        }
        return false;
    }
}
