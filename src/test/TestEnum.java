package test;

import java.util.ArrayList;

public class TestEnum {

    public static void main(String[] args) {
        ArrayList<Command> commands = new ArrayList<>();

        commands.add(Command.LEFT);
        commands.add(Command.RIGHT);

        for (Command command : commands) {
            System.out.println(command);
        }
    }
}
