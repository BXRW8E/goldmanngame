package goldmann.tools.network;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

/**
 *
 */
public final class GetIPV4Address {

    /**
     * Function get ipadresses. If there ara a LAN
     * @return returns a list of ips
     */
    public final static ArrayList<String> getIpAddress() {
        ArrayList<String> ips = new ArrayList<String>();

        Enumeration e = null;
        try {
            e = NetworkInterface.getNetworkInterfaces();
        } catch (SocketException e1) {
            e1.printStackTrace();
        }
        while (e.hasMoreElements()) {
            NetworkInterface n = (NetworkInterface) e.nextElement();
            Enumeration ee = n.getInetAddresses();
            while (ee.hasMoreElements()) {
                InetAddress i = (InetAddress) ee.nextElement();
                if (i.getHostAddress().toString().startsWith("127.") ||
                        i.getHostAddress().toString().startsWith("0:") ||
                        i.getHostAddress().toString().contains(":")) {
                    continue;
                }
                ips.add(i.getHostAddress());
            }
        }
        return ips;
    }
}
