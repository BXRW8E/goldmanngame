package goldmann.GUI;

import goldmann.networking.Network;
import goldmann.tools.CharacterNameEnum;
import goldmann.tools.GameStates;
import goldmann.tools.misc.StringToEnum;
import goldmann.tools.network.CheckIPFormat;
import goldmann.tools.network.GetIPV4Address;
import javafx.animation.AnimationTimer;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.concurrent.ThreadLocalRandom;

public class ControllerStart implements Initializable {
    private GraphicsContext backgroundContext;
    private GraphicsContext characterContext;
    private GraphicsContext shadowContext;
    private GraphicsContext thinkContext;
    private String ipOwn;
    // private TCPServer tcpServer;
    // private TCPClient tcpClient;
    private Network network;
    private boolean thinkCloudShouldRun;
    private boolean isCreated, isConnected = false;

    private AppLoop appLoop;

    @FXML
    RadioButton serverButton, clientButton;

    @FXML
    Canvas background, characterCanvas, characterShadowCanvas, thinkCanvas, messageLineCanvas;

    @FXML
    TextField nameField, ipAddressField, passwordField;

    @FXML
    ComboBox characterCombo;

    @FXML
    ToggleButton connectButton;

    // @FXML
    //Button connectButton;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        System.out.println("Loading graphics....");
        drawStartPage();
    }

    @FXML
    public void setServer() {
        nameField.setText("Player1");
        ipAddressField.setText(ipOwn);
        ipAddressField.setDisable(true);
        Integer randomNum = ThreadLocalRandom.current().nextInt(1000, 9999 + 1);
        passwordField.setText(randomNum.toString());
        passwordField.setDisable(true);
        connectButton.setText("Start");
    }

    @FXML
    public void setClient() {
        nameField.setText("Player2");
        ipAddressField.setDisable(false);
        ipAddressField.setText("");
        passwordField.setDisable(false);
        passwordField.setText("");
        EventHandler<ActionEvent> actionEventEventHandler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                passwordField.selectAll();
            }
        };
        passwordField.setOnAction(actionEventEventHandler);
        connectButton.setText("Connect");
    }

    /**
     * @author mate
     * Creates a server or client. It depends on wich is selected button is selected
     */
    @FXML
    public void setConnect() {
        if (characterCombo.getSelectionModel().isEmpty()) {
            characterCombo.getSelectionModel().select("RED");
        }

        if (connectButton.isSelected()) {
            connectButton.setText("Stop");
            appLoop.setGameState(GameStates.CONNECTING);
            setFeedbackMessage("Waiting for connection...");
            disableFields();


//            if (serverButton.isSelected()) {
//            } else
            if (clientButton.isSelected()) {

                if (CheckIPFormat.checkIPFormat(ipAddressField.getText())) {            // check for ip format
                    isCreated = true;
                    setFeedbackMessage("Waiting for connection...");
                    disableFields();
                } else {
                    System.out.println("Wrong ip format");              // if ip format is bad write it to screen
                    setFeedbackMessage("Wrong IP format");
                    connectButton.setText("Connect");                   // set text to connect
                    connectButton.setSelected(false);
                    enableFieldsClient();
                    appLoop.setGameState(GameStates.STARTED);
                }
            }
        } else {
            if (serverButton.isSelected()) {
                connectButton.setText("Start");
                if (isCreated == true) {
                    isCreated = false;
                }
                enableFieldsServer();
            } else {
                enableFieldsClient();
                connectButton.setText("Connect");
                if (isCreated == true) {
                    isCreated = false;
                }
            }
            appLoop.setGameState(GameStates.STARTED);
            setFeedbackMessage("Connection closed");
        }

    }

    public AppLoop getAppLoop() {
        return appLoop;
    }

    public void setAppLoop(AppLoop appLoop) {
        this.appLoop = appLoop;
    }

    /**
     * @author mate
     */
    public void disableFields() {
        nameField.setDisable(true);
        serverButton.setDisable(true);
        clientButton.setDisable(true);
        ipAddressField.setDisable(true);
        passwordField.setDisable(true);
        characterCombo.setDisable(true);
    }

    /**
     * @author mate
     */
    public void enableFieldsClient() {
        nameField.setDisable(false);
        serverButton.setDisable(false);
        clientButton.setDisable(false);
        ipAddressField.setDisable(false);
        passwordField.setDisable(false);
        characterCombo.setDisable(false);
    }

    /**
     * @author mate
     */
    public void enableFieldsServer() {
        nameField.setDisable(false);
        serverButton.setDisable(false);
        clientButton.setDisable(false);
        ipAddressField.setDisable(true);
        passwordField.setDisable(true);
        characterCombo.setDisable(false);
    }

    public Network getNetwork() {
        return network;
    }

    public void setNetwork(Network network) {
        this.network = network;
    }

    public boolean isCreated() {
        return isCreated;
    }

    public boolean isConnected() {
        return isConnected;
    }


    @FXML
    public void onNameClick() {
        nameField.selectAll();
    }

    @FXML
    public void onIPClick() {
        ipAddressField.selectAll();
    }

    @FXML
    public void onPasswordClick() {
        passwordField.selectAll();
    }

    @FXML
    public void comboBoxOnAction() {
        characterShadowCanvas.toFront();
        characterCanvas.toFront();
        characterContext = characterCanvas.getGraphicsContext2D();
        shadowContext = characterShadowCanvas.getGraphicsContext2D();

        Object selected = characterCombo.getValue();

        if (selected == "RED") {
            Image charImage = new Image("character1/standingr.png", 150, 300, true, false);
            characterContext.clearRect(0, 0, characterCanvas.getWidth(), characterCanvas.getHeight());
            shadowContext.clearRect(0, 0, characterShadowCanvas.getWidth(), characterShadowCanvas.getHeight());
            characterContext.drawImage(charImage, 0, 0);
            shadowContext.drawImage(charImage, 0, 0);
            System.out.println("RED selected");
        } else if (selected == "BLUE") {
            Image charImage = new Image("character2/standingr.png", 150, 300, true, false);
            characterContext.clearRect(0, 0, characterCanvas.getWidth(), characterCanvas.getHeight());
            shadowContext.clearRect(0, 0, characterShadowCanvas.getWidth(), characterShadowCanvas.getHeight());
            characterContext.drawImage(charImage, 0, 0);
            shadowContext.drawImage(charImage, 0, 0);
            System.out.println("BLUE selected");
        } else { // same as RED
            Image charImage = new Image("character1/standingr.png", 150, 300, true, false);
            characterContext.clearRect(0, 0, characterCanvas.getWidth(), characterCanvas.getHeight());
            shadowContext.clearRect(0, 0, characterShadowCanvas.getWidth(), characterShadowCanvas.getHeight());
            characterContext.drawImage(charImage, 0, 0);
            shadowContext.drawImage(charImage, 0, 0);
            System.out.println("else selected");
        }
        showThinkCloud();
    }

    public void showThinkCloud() {
        thinkCloudShouldRun = true;
        thinkContext = thinkCanvas.getGraphicsContext2D();
        AnimatedImage thinkCloud = new AnimatedImage();
        Image[] imageArray = new Image[4];
        for (int i = 0; i < 4; i++) {
            int j = i + 1;
            imageArray[i] = new Image("think_" + j + ".png", 215, 110, true, false);
        }
        thinkCloud.frames = imageArray;
        thinkCloud.duration = 2.0;

        final long startNanoTime = System.nanoTime();

        new AnimationTimer() {
            public void handle(long currentNanoTime) {
                if (thinkCloudShouldRun) {
                    double t = (currentNanoTime - startNanoTime) / 1000000000.0;
                    thinkContext.drawImage(thinkCloud.getFrame(t), 0, 0);
                }
            }
        }.start();
    }

    public void drawStartPage() {
        backgroundContext = background.getGraphicsContext2D();
        Image image = new Image("goldmann-2.jpg"); //,1066,600,false,true);

        characterCombo.setItems(FXCollections.observableArrayList(
                "RED", "BLUE"));
        backgroundContext.drawImage(image, 0, 0); //,1066,600);
        backgroundContext.setFill(Color.YELLOW);
        backgroundContext.setStroke(Color.RED);
        backgroundContext.setLineWidth(3);
        Font titel = Font.font("Lucida Sans Typewriter", FontWeight.BOLD, 68); //Agency FB
        Font subtitel = Font.font("Lucida Sans Typewriter", FontWeight.BOLD, 20);
        backgroundContext.setFont(titel);
        backgroundContext.fillText("Welcome to Goldmann Game!", 29, 150);
        backgroundContext.strokeText("Welcome to Goldmann Game!", 25, 150);
        backgroundContext.setFont(subtitel);
        backgroundContext.setLineWidth(1);
        backgroundContext.fillText("Please Select!", 438, 225);
        backgroundContext.strokeText("Please Select!", 438, 225);
        backgroundContext.fillText("Name:", 438, 305);
        backgroundContext.strokeText("Name:", 438, 305);
        backgroundContext.fillText("IP:", 438, 385);
        backgroundContext.strokeText("IP:", 438, 385);
        backgroundContext.fillText("Password:", 438, 465);
        backgroundContext.strokeText("Password:", 438, 465);

        // Group
        ToggleGroup group = new ToggleGroup();
        //Server radiobutton
        serverButton.setToggleGroup(group);
        serverButton.setSelected(true);
        connectButton.setText("Start");
        //Client radiobutton
        clientButton.setToggleGroup(group);
        ArrayList<String> ipList = new ArrayList<>(GetIPV4Address.getIpAddress());
        ipOwn = ipList.get(0);
        Font ipFontStyle = Font.font("Lucida Sans Typewriter", FontWeight.SEMI_BOLD, 12);
        backgroundContext.setFont(ipFontStyle);
        backgroundContext.setFill(Color.WHITE);
        backgroundContext.fillText("Your IP:" + ipOwn, 900, 20);
        ipAddressField.setText(ipOwn);
        ipAddressField.setDisable(true);
        Integer randomNum = ThreadLocalRandom.current().nextInt(1000, 9999 + 1);
        passwordField.setText(randomNum.toString());
        passwordField.setDisable(true);
    }

    // TODO: Máté ezzel tudsz üzenetet kiírni start scene bal felső sarkába
    public void setFeedbackMessage(String feedbackMessage) {
        GraphicsContext messageLineContext = messageLineCanvas.getGraphicsContext2D();
        messageLineContext.clearRect(0, 0, messageLineCanvas.getWidth(), messageLineCanvas.getHeight());
        Font messageFontStyle = Font.font("Lucida Sans Typewriter", FontWeight.SEMI_BOLD, 12);
        messageLineContext.setFont(messageFontStyle);
        messageLineContext.setFill(Color.WHITE);
        messageLineContext.fillText(feedbackMessage, 20, 20);
    }


    public CharacterNameEnum getCaharacterType() {
        return StringToEnum.getEnumFromString(CharacterNameEnum.class, characterCombo.getValue().toString());
    }

    public String getUserName() {
        return nameField.getText();
    }

    public GraphicsContext getBackgroundContext() {
        return backgroundContext;
    }

    public boolean isThinkCloudShouldRun() {
        return thinkCloudShouldRun;
    }

    public void setThinkCloudShouldRun(boolean thinkCloudShouldRun) {
        this.thinkCloudShouldRun = thinkCloudShouldRun;
    }

    public boolean isServer() {
        if (serverButton.isSelected()) {
            return true;
        }
        return false;
    }

    public String getPassword() {
        return passwordField.getText();
    }
}

