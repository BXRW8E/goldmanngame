package goldmann.tools.network;

import java.util.regex.Pattern;

public class CheckIPFormat {

    /**
     * Checks the ip format of a String
     * @param ip String wich must be checked
     * @return true if the @param ip is an correct ip address
     */
    public static boolean checkIPFormat(String ip) {
        boolean isOkay = false;
//        String regex = "(\\d+)\\.(\\d+)\\.(\\d+)\\.(\\d+)";
        String regex = "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$";
        isOkay = Pattern.matches(regex, ip);
        return isOkay;
    }

    public static void main(String[] args) {
        String[] ips = {"192.168.0.1", "192.168.0.17", "192.1.1.1", "alma", "0.0.0.0" ,"", "123.123", };
        for (int i = 0; i < ips.length; i++) {
            System.out.println(checkIPFormat(ips[i]));
        }
    }

}
