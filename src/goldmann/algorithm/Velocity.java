package goldmann.algorithm;

public class Velocity {
    private double velocityX;
    private double velocityY;

    /**
     * Responsible for the moving speed of a character which is always constant.
     */
    public Velocity() {
        velocityX = 0;
        velocityY = 0;
    }
    public void setVelocity(double x, double y) {
        velocityX = x;
        velocityY = y;
    }

    public void addVelocity(double x, double y) {
        velocityX += x;
        velocityY += y;
    }

    public double getVelocityX() {
        return velocityX;
    }

    public double getVelocityY() {
        return velocityY;
    }
}

