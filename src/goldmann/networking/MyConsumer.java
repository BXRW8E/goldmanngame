package goldmann.networking;

import goldmann.tools.json.GGMEssage;
import goldmann.tools.json.JsonParserGG;

public class MyConsumer {

    public static  void printmsg(String msg){
        System.out.println(msg);
    }

    public static  void password(String msg){
        System.out.println("Password received");
    }

    public static void receiveGGMessage(String jsonString){
        GGMEssage ggmEssage = JsonParserGG.toGGMessage(jsonString);
        System.out.println(ggmEssage);
    }

}
