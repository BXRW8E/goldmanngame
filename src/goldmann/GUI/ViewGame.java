package goldmann.GUI;

import goldmann.tools.CharacterNameEnum;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class ViewGame implements Initializable {

    private GraphicsContext gameContext;
    private GraphicsContext statusContext;
    private GraphicsContext backgroundContext;
    private GraphicsContext timerContext;
    private GraphicsContext messageContext;
    private GraphicsContext messageTitleContext;
    private GraphicsContext messageSubTitleContext;
    private ArrayList<String> commands;
    private int password;
    //private static int id = 0;
    //private int idOwn;
    private boolean hitEnable;
    private boolean isPauseSelected;
    private boolean isContinueSelected;
    private boolean isExitToMainSelected;
    private boolean isRestartSelected;


    @FXML
    Canvas backGround, gameField, statusLayer, messageLayer, messageTitleLayer, messageSubTitleLayer, timerLayer;

    // TODO: Örkény ezekre a gombokra kell eventet csinálnod!!!
    @FXML
    Button pauseButton, continueFightButton, exitToMainButton, restartButton;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        System.out.println("Viewgame loaded.");
        initViewGame();
    }

    /**
     * This function is responsible for initializing values and set the start status of the game window locally before the fight starts
     */
    public void initViewGame() {
        commands = new ArrayList<String>();
        hitEnable = false;
        isPauseSelected = false;
        isContinueSelected = false;
        isExitToMainSelected = false;
        isRestartSelected = false;
        //id++;
        //idOwn = id;
        gameContext = gameField.getGraphicsContext2D(); // set all the contexts from canvas layers
        statusContext = statusLayer.getGraphicsContext2D();
        backgroundContext = backGround.getGraphicsContext2D();
        timerContext = timerLayer.getGraphicsContext2D();
        messageLayer.setOpacity(0.8);
        messageContext = messageLayer.getGraphicsContext2D();
        messageTitleContext = messageTitleLayer.getGraphicsContext2D();
        messageSubTitleContext = messageSubTitleLayer.getGraphicsContext2D();
        restartButton.setVisible(false); // make sure pause menu's buttons are invisible
        exitToMainButton.setVisible(false);
        continueFightButton.setVisible(false);
        pauseButton.setVisible(true); // display pause button
        Image background;
        password = AppLoop.getPassword();
        System.out.println(password);
        if (password < 5500) { // here comes the "if password.parseInt < 5000"
            background = new Image("goldmann_plat_norm.png");
        } else { // background image is set regarding to the password value
            background = new Image("goldmann_plat_workout.png");
        }
        backgroundContext.drawImage(background, 0, 0);
    }

    /**
     * @param p1     contains data of player1, Player class format
     * @param p2     same as p1
     * @param time   the actual timer value converted to string
     * @param server decides position of players on the board (convention in placing player names in the game field)
     * @author Gábor
     * this is called in every iteration of the Apploop of the game, responsible for refreshing the game field
     */
    public void rePaint(Player p1, Player p2, String time, boolean server) {
        gameContext.clearRect(0, 0, gameField.getWidth(), gameField.getHeight());
        if (p2.getCharacter() != null && gameContext != null) {
            p2.render(gameContext);
        }
        p1.render(gameContext);
        statusContext.clearRect(0, 0, statusLayer.getWidth(), statusLayer.getHeight());
        drawNames(p1, p2, server);
        paintClock(time);
    }

    /**
     * @param p1     same as in repaint
     * @param p2     same a in repaint
     * @param server same as in repaint
     * @author Gábor
     * Resoponsible for drawing the names and lifelines on the screen
     */
    public void drawNames(Player p1, Player p2, boolean server) {
        Font subtitel = Font.font("Lucida Sans Unicode", 32);
        statusContext.setFont(subtitel);
        statusContext.setFill(Color.YELLOW);
        statusContext.setStroke(Color.RED);
        statusContext.setLineWidth(1.0); // 1.0 linewidth for text
        if (server) {
            statusContext.fillText(p1.getUserName(), 50, 40);
            statusContext.strokeText(p1.getUserName(), 50, 40);
            statusContext.fillText(p2.getUserName(), statusLayer.getWidth() - 250, 40);
            statusContext.strokeText(p2.getUserName(), statusLayer.getWidth() - 250, 40);
            statusContext.setLineWidth(5.0); // 5.0 linewidth for lifestate rectangle
            statusContext.strokeRect(50, 50, 200, 30);
            statusContext.fillRect(50, 50, 2 * p1.getLifeState(), 30);
            statusContext.strokeRect(statusLayer.getWidth() - 250, 50, 200, 30);
            statusContext.fillRect(statusLayer.getWidth() - 250, 50, 2 * p2.getLifeState(), 30);
        } else {
            statusContext.fillText(p2.getUserName(), 50, 40);
            statusContext.strokeText(p2.getUserName(), 50, 40);
            statusContext.fillText(p1.getUserName(), statusLayer.getWidth() - 250, 40);
            statusContext.strokeText(p1.getUserName(), statusLayer.getWidth() - 250, 40);
            statusContext.setLineWidth(5.0);
            statusContext.strokeRect(50, 50, 200, 30);
            statusContext.fillRect(50, 50, 2 * p2.getLifeState(), 30);
            statusContext.strokeRect(statusLayer.getWidth() - 250, 50, 200, 30);
            statusContext.fillRect(statusLayer.getWidth() - 250, 50, 2 * p1.getLifeState(), 30);
        }

    }

    /**
     * @param time
     * @author Gábor
     * Responsible for displaying the remaining time in the game
     */
    public void paintClock(String time) {
        Font clock = Font.font("Lucida Sans Unicode", 50);
        timerContext.setFont(clock);
        timerContext.setFill(Color.YELLOW);
        timerContext.setStroke(Color.RED);
        timerContext.clearRect(0, 0, timerLayer.getWidth(), timerLayer.getHeight());
        timerContext.fillText(time, 0, 50);
        timerContext.strokeText(time, 0, 50);
    }

//TODO: Örkény Ezt hívod a pause megjelenítéséhez

    /**
     * It is responsible for displaying the Pause menu regarding to ownClicked parameter
     * Controller calls this after buttonpress
     *
     * @param ownClicked shows if pause was pressed in this view or in the other one
     * @author Gábor
     */
    public void displayPaused(boolean ownClicked) {
        pauseButton.setVisible(false);
        messageContext.setFill(Color.LIGHTBLUE);
        messageContext.fillRect(20, 20, 1026, 560);
        restartButton.setVisible(true);
        exitToMainButton.setVisible(true);
        continueFightButton.setVisible(true);
        Font titleFont = Font.font("Lucida Sans Unicode", 40);
        Font instructionFont = Font.font("Lucida Sans Unicode", 24);
        String titleString;
        String instructionString;
        if (ownClicked) {
            titleString = "You paused the fight.";
            instructionString = "Press continue or choose one from the \n                  other buttons!";
        } else {
            continueFightButton.setDisable(true);
            titleString = "Your partner paused the fight.";
            instructionString = "Wait until the game continues or choose\n          one from the available buttons!";
        }
        messageTitleContext.setFont(titleFont);
        messageTitleContext.setFill(Color.YELLOW);
        messageTitleContext.setStroke(Color.RED);
        messageTitleContext.fillText(titleString, 0, 80);
        messageTitleContext.strokeText(titleString, 0, 80);
        messageSubTitleContext.setFont(instructionFont);
        messageSubTitleContext.setFill(Color.YELLOW);
        messageSubTitleContext.setStroke(Color.RED);
        messageSubTitleContext.fillText(instructionString, 0, 60);
        messageSubTitleContext.strokeText(instructionString, 0, 60);
    }

    //TODO: Örkény ezzel tudod eltűntetni a kirajzolt pause/EndOfTheGame menüt
    //EoG: end of the game

    /**
     * @author Gábor
     * With this method, gamefield will be shown, Pause menu or EoG menu disappears
     */
    public void clearPauseOrEoGDisplay() {
        pauseButton.setVisible(true);
        messageContext.clearRect(0, 0, messageLayer.getWidth(), messageLayer.getHeight());
        messageTitleContext.clearRect(0, 0, messageTitleLayer.getWidth(), messageTitleLayer.getHeight());
        messageSubTitleContext.clearRect(0, 0, messageSubTitleLayer.getWidth(), messageSubTitleLayer.getHeight());
        restartButton.setVisible(false);
        exitToMainButton.setVisible(false);
        continueFightButton.setVisible(false);
        continueFightButton.setDisable(false);
    }

    /**
     * @param win      true, if player of the view won the game
     * @param timeIsUp true, if time was up without any winner
     * @author Gábor
     * similar to the pause menu, it interrupts the game play in game filed
     * it handles different results of the fight
     * possible ways, when a game is ended:
     * win
     * loose
     * time is up
     * connection lost
     */
    public void displayEndOfTheGame(boolean win, boolean timeIsUp) {
        pauseButton.setVisible(false);
        messageContext.setFill(Color.LIGHTBLUE);
        messageContext.fillRect(20, 20, 1026, 560);
        restartButton.setVisible(true);
        exitToMainButton.setVisible(true);
        Font titleFont = Font.font("Lucida Sans Unicode", 40);
        Font instructionFont = Font.font("Lucida Sans Unicode", 24);
        String titleString;
        String instructionString;
        instructionString = "To start new fight, press Restart button! You can \nreach start window with exit button.";
        if (win && !timeIsUp) { // win
            titleString = "Congratulations, you won!";
        } else if (!timeIsUp && !win) { // loose
            titleString = "You Loose!";
        } else if (timeIsUp && !win) { // time is up
            titleString = "Time is up!";
        } else { // connection broken
            titleString = "Your connection is broken!";
            instructionString = "Press Exit to main!";
            restartButton.setDisable(true);
        }
        messageTitleContext.setFont(titleFont);
        messageTitleContext.setFill(Color.YELLOW);
        messageTitleContext.setStroke(Color.RED);
        messageTitleContext.fillText(titleString, 0, 80);
        messageTitleContext.strokeText(titleString, 0, 80);
        messageSubTitleContext.setFont(instructionFont);
        messageSubTitleContext.setFill(Color.YELLOW);
        messageSubTitleContext.setStroke(Color.RED);
        messageSubTitleContext.clearRect(0, 0, messageSubTitleLayer.getWidth(), messageSubTitleLayer.getHeight());
        messageSubTitleContext.fillText(instructionString, 0, 60);
        messageSubTitleContext.strokeText(instructionString, 0, 60);
    }

    public void setInstructionString(String instructionString) {
        Font instructionFont = Font.font("Lucida Sans Unicode", 24);
        messageSubTitleContext.setFont(instructionFont);
        messageSubTitleContext.setFill(Color.YELLOW);
        messageSubTitleContext.setStroke(Color.RED);
        messageSubTitleContext.clearRect(0, 0, messageSubTitleLayer.getWidth(), messageSubTitleLayer.getHeight());
        messageSubTitleContext.fillText(instructionString, 0, 60);
        messageSubTitleContext.strokeText(instructionString, 0, 60);
    }

    //TODO: (Örkény) az alábbi függvényeket SceneBuilderben hozzárendeltem a gombokhoz, ezekbe írhatod az implementációjukat
    public void setPauseButton() {
//        displayPaused(true);
//        commands.add("PAUSE");
//        System.out.println("PAUSE");
        isPauseSelected = true;
    }

    public void setExitToMainButton() {
        isExitToMainSelected = true;
    }

    public void setRestartButton() {
        isRestartSelected = true;
    }

    public void setContinueFightButton() {
        isContinueSelected = true;
    }

    public final EventHandler<? super KeyEvent> gameWindowKeyPressed(KeyEvent e) {
        String code = e.getCode().toString();
        if (!commands.contains(code))
            commands.add(code);
        return null;
    }

    public final EventHandler<? super KeyEvent> gameWindowKeyReleased(KeyEvent e) {
        String code = e.getCode().toString();
        if (code.equals("CONTROL")) {
            hitEnable = true;
        }
        commands.remove(code);
        return null;
    }

    public ArrayList<String> getCommands() {
        return commands;
    }

    public boolean isHitEnable() {
        return hitEnable;
    }

    public void setHitEnable(boolean hitEnable) {
        this.hitEnable = hitEnable;
    }

    public boolean isPauseSelected() {
        return isPauseSelected;
    }

    public void setPauseSelected(boolean pauseSelected) {
        this.isPauseSelected = pauseSelected;
    }

    public boolean isContinueSelected() {
        return isContinueSelected;
    }

    public void setContinueSelected(boolean continueSelected) {
        isContinueSelected = continueSelected;
    }

    public boolean isExitToMainSelected() {
        return isExitToMainSelected;
    }

    public void setExitToMainSelected(boolean exitToMainSelected) {
        isExitToMainSelected = exitToMainSelected;
    }

    public boolean isRestartSelected() {
        return isRestartSelected;
    }

    public void setRestartSelected(boolean restartSelected) {
        isRestartSelected = restartSelected;
    }

    public void setRestartDisabled(boolean isDisabled) {
        restartButton.setDisable(isDisabled);
    }

    public int getPassword() {
        return password;
    }

    public void setPassword(int password) {
        this.password = password;
    }
}
