package goldmann.GUI;

public class Position {
    private double x;
    private double y;
    private boolean direction; // true to right false to left

    public Position(double x, double y, boolean direction) {
        this.direction = direction;
        this.y = y;
        if (x > 1066) {
            this.x = 1066;
        } else if (x < 0) {
            this.x = 1066;
        } else
            this.x = x;
    }

    public Position() {
        x = 0;
        y = 0;
        direction = true;
    }

    public void setX(double x) {
        if (x > 950) {
            this.x = 950;
        } else if (x < 0) {
            this.x = 0;
        } else
            this.x = x;
    }

    public double getX() {
        return x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getY() {
        return y;
    }

    public void setDirection(boolean direction) {
        this.direction = direction;
    }

    public boolean isDirection() {
        return direction;
    }

    @Override
    public String toString() {
        return "Position{" +
                "x=" + x +
                ", y=" + y +
                ", direction=" + direction +
                '}';
    }
}
