package sample;

import javafx.animation.AnimationTimer;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    private Main mainApp;
    private AppLoop appLoop;
    private GraphicsContext backgroundContext;
    private GraphicsContext gameContext;


    @FXML
    Canvas backGround;

    @FXML
    Canvas gameField;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        System.out.println("Loading graphics....");
        backgroundContext = backGround.getGraphicsContext2D();
        gameContext = gameField.getGraphicsContext2D();

        Image image = new Image("file:resources/jatszoter3.jpg");
        backgroundContext.drawImage(image, 0, 0);
    }

    public void printHello() {
        System.out.println("hello world ");
    }

    public void setMainApp(Main mainApp) {
        this.mainApp = mainApp;
    }

    public void setAnimationTimer(AppLoop appLoop) {
        this.appLoop = appLoop;
    }


    public void changeBackGround(String url) {
        Image image = new Image(url);
        backgroundContext.clearRect(0, 0, backGround.getWidth(), backGround.getHeight());
        backgroundContext.drawImage(image, 0, 0);
    }

    public Canvas getBackGround() {
        return backGround;
    }

    public Canvas getGameField() {
        return gameField;
    }

    public void drawFigure(Image figure, double x, double y) {
        gameContext.clearRect(0, 0, gameField.getWidth(), gameField.getHeight());
        gameContext.drawImage(figure, x, y);
    }

    public void drawFigure(GraphicsContext graphicsContext, Image figure, double x, double y) {
        graphicsContext.clearRect(0, 0, gameField.getWidth(), gameField.getHeight());
        graphicsContext.drawImage(figure, x, y);
    }

    public GraphicsContext getGameContext() {
        return gameContext;
    }

    public GraphicsContext getBackgroundContext() {
        return backgroundContext;
    }

}

