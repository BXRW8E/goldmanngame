package goldmann.GUI;

import goldmann.tools.CharacterNameEnum;
import goldmann.tools.StateOfPlayer;
import javafx.geometry.Rectangle2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.shape.Rectangle;
import goldmann.algorithm.ImageContainer;
import goldmann.tools.Jump_Tool;
import goldmann.algorithm.Velocity;

public class Character {
    private CharacterNameEnum name; // RED or BLUE
    private Position characterPosition;
    private ImageContainer mycont;
    private Image activeImage;
    private StateOfPlayer state;
    private Velocity velocity;

    public Character() {
        name = CharacterNameEnum.RED;
        mycont = new ImageContainer(name);
        //setImages();
    }

    public Character(CharacterNameEnum name, Position characterPosition) {
        this.name = name;
        this.mycont = new ImageContainer(name);
        this.characterPosition = characterPosition;
        if (characterPosition.isDirection()) {
            state = StateOfPlayer.STANDING_R;
        } else {
            state = StateOfPlayer.STANDING_L;
        }
        this.setActiveImage(state, 0.0);
        this.velocity = new Velocity();
    }

    public Rectangle2D getHitBox() {
        double y = characterPosition.getY() + 70;
        double x = characterPosition.getX() - 25;
        if (characterPosition.isDirection())
            x += activeImage.getWidth();
        return new Rectangle2D(x, y, 25, 25);
    }

    public void showHitBox(GraphicsContext gc) {
        double y = characterPosition.getY() + 70;
        double x = characterPosition.getX() - 25;
        if (characterPosition.isDirection())
            x += activeImage.getWidth();
        gc.fillRect(x, y, 50, 50);
    }

    public Rectangle2D getBoundary() {
        double x = characterPosition.getX();
        double y = characterPosition.getY();
        return new Rectangle2D(x, y, activeImage.getWidth(), activeImage.getHeight());
    }

    public void showBoundary(GraphicsContext gc) {
        double x = characterPosition.getX();
        double y = characterPosition.getY();
        gc.fillRect(x, y, activeImage.getWidth(), activeImage.getHeight());
    }

    public Position getCharacterPosition() {
        return characterPosition;
    }

    public void setCharacterPosition(Position characterPosition) {
        this.characterPosition = characterPosition;
    }

    public boolean getDirection() {
        return characterPosition.isDirection();
    }

    public CharacterNameEnum getName() {
        return name;
    }

    public void setName(CharacterNameEnum name) {
        this.name = name;
    }

    public void setState(StateOfPlayer state, double t) {
        this.state = state;
        setActiveImage(state, t);
        setVelocity();
    }
    public void setenemyState(StateOfPlayer state, double t) {
        this.state = state;
        setActiveImage(state, t);
    }

    public StateOfPlayer getState() {
        return state;
    }

    public Velocity getVelocity() {
        return velocity;
    }

    public void setVelocity() {

        switch (this.state) {

            case STEP_L:
                velocity.setVelocity(-300, 0);
                characterPosition.setDirection(false);
                break;
            case STEP_R:
                velocity.setVelocity(300, 0);
                characterPosition.setDirection(true);
                break;
            case STANDING_L:
                characterPosition.setDirection(false);
                velocity.setVelocity(0, 0);
                break;
            case STANDING_R:
                characterPosition.setDirection(true);
                velocity.setVelocity(0, 0);
                break;
            case PUNCH_L:
                characterPosition.setDirection(false);
                velocity.setVelocity(0, 0);
                break;
            case PUNCH_R:
                characterPosition.setDirection(true);
                velocity.setVelocity(0, 0);
                break;
            case PUNCH_STEP_L:
                velocity.setVelocity(-300, 0);
                break;
            case PUNCH_STEP_R:
                velocity.setVelocity(300, 0);
                break;
            case LOSE_L:
                characterPosition.setY(500);
                velocity.setVelocity(0, 0);
                break;
            case WIN_L:
                velocity.setVelocity(0, 0);
                break;
        }

    }

    public void update(double time) {
        double x = characterPosition.getX() + velocity.getVelocityX() * time;
        double y = characterPosition.getY() + velocity.getVelocityY() * time;
        characterPosition.setX(x);
        characterPosition.setY(y);
    }

    public Image getActiveImage() {
        return activeImage;
    }

    public void setActiveImage(StateOfPlayer state, double t) {
        switch (state) {

            case STEP_L:
                activeImage = mycont.getStepl(t);
                break;
            case STEP_R:
                activeImage = mycont.getStepr(t);
                break;
            case STANDING_L:
                activeImage = mycont.getStandl();
                break;
            case STANDING_R:
                activeImage = mycont.getStandr();
                break;
            case PUNCH_L:
                activeImage = mycont.getPunch_l();
                break;
            case PUNCH_R:
                activeImage = mycont.getPunch_r();
                break;
            case PUNCH_STEP_L:
                activeImage = mycont.getSteppunch_l();
                break;
            case PUNCH_STEP_R:
                activeImage = mycont.getSteppunch_r();
                break;
            case LOSE_L:
                activeImage = mycont.getLoose();
                break;
            case WIN_L:
                activeImage = mycont.getWin();
                break;
            default:
                break;

        }

    }

    public boolean getHitted(Character c) {
        return c.getHitBox().intersects(this.getBoundary());
    }

}
