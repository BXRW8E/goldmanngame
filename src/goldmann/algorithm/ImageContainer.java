package goldmann.algorithm;

import javafx.scene.image.Image;
import goldmann.GUI.AnimatedImage;
import goldmann.tools.CharacterNameEnum;

public class ImageContainer {
    Image standr;
    Image standl;
    AnimatedImage stepr;
    AnimatedImage stepl;
    Image[] imageArray_r;
    Image[] imageArray_l;
    Image steppunch_r;
    Image punch_r;
    Image punch_l;
    Image steppunch_l;
    Image win;
    Image loose;
    CharacterNameEnum type;

    /**
     * Initialize all of the images which are needed for a character during the game.
     * @param type: RED or BLUE character
     */
    public ImageContainer(CharacterNameEnum type) {
        this.type = type;
        stepr = new AnimatedImage();
        stepl = new AnimatedImage();
        imageArray_r = new Image[4];
        imageArray_l = new Image[4];
        switch (type) {
            case RED:
                standr = new Image("character1/standingr.png", 150, 300, true, false);
                standl = new Image("character1/standingl.png", 150, 300, true, false);
                punch_r = new Image("character1/punchr.png", 150, 300, true, false);
                punch_l = new Image("character1/punchl.png", 150, 300, true, false);
                imageArray_r[0] = new Image("character1/step1r.png", 150, 300, true, false);
                imageArray_r[1] = new Image("character1/step2r.png", 150, 300, true, false);
                imageArray_r[2] = new Image("character1/step3r.png", 150, 300, true, false);
                imageArray_r[3] = new Image("character1/step4r.png", 150, 300, true, false);
                steppunch_r = new Image("character1/punchnstepr.png", 150, 300, true, false);
                imageArray_l[0] = new Image("character1/step1l.png", 150, 300, true, false);
                imageArray_l[1] = new Image("character1/step2l.png", 150, 300, true, false);
                imageArray_l[2] = new Image("character1/step3l.png", 150, 300, true, false);
                imageArray_l[3] = new Image("character1/step4l.png", 150, 300, true, false);
                steppunch_l = new Image("character1/punchnstepl.png", 150, 300, true, false);
                win = new Image("character1/winl.png", 150, 300, true, false);
                loose = new Image("character1/losel.png", 300, 150, true, false);
                break;
            case BLUE:
                standr = new Image("character2/standingr.png", 150, 300, true, false);
                standl = new Image("character2/standingl.png", 150, 300, true, false);
                punch_r = new Image("character2/punchr.png", 150, 300, true, false);
                punch_l = new Image("character2/punchl.png", 150, 300, true, false);
                imageArray_r[0] = new Image("character2/step1r.png", 150, 300, true, false);
                imageArray_r[1] = new Image("character2/step2r.png", 150, 300, true, false);
                imageArray_r[2] = new Image("character2/step3r.png", 150, 300, true, false);
                imageArray_r[3] = new Image("character2/step4r.png", 150, 300, true, false);
                steppunch_r = new Image("character2/punchnstepr.png", 150, 300, true, false);
                imageArray_l[0] = new Image("character2/step1l.png", 150, 300, true, false);
                imageArray_l[1] = new Image("character2/step2l.png", 150, 300, true, false);
                imageArray_l[2] = new Image("character2/step3l.png", 150, 300, true, false);
                imageArray_l[3] = new Image("character2/step4l.png", 150, 300, true, false);
                steppunch_l = new Image("character2/punchnstepl.png", 150, 300, true, false);
                win = new Image("character2/winl.png", 150, 300, true, false);
                loose = new Image("character2/losel.png", 300, 150, true, false);
                break;
            default:
                System.out.println("Images failed to load.");
                break;

        }
        stepr.frames = imageArray_r;
        stepr.duration = 0.4;
        stepl.frames = imageArray_l;
        stepl.duration = 0.4;
    }

    public Image getStandr() {
        return standr;
    }

    public Image getStandl() {
        return standl;
    }


    public Image getSteppunch_r() {
        return steppunch_r;
    }

    public Image getSteppunch_l() {
        return steppunch_l;
    }

    public Image getWin() {
        return win;
    }

    public Image getLoose() {
        return loose;
    }

    public Image getPunch_r() {
        return punch_r;
    }

    public Image getPunch_l() {
        return punch_l;
    }

    public Image getStepr(double t) {
        return stepr.getFrame(t);
    }

    public Image getStepl(double t) {
        return stepl.getFrame(t);
    }
}

