package goldmann.tools.json;

import goldmann.GUI.Position;
import goldmann.tools.CharacterNameEnum;
import goldmann.tools.GameStates;
import goldmann.tools.StateOfPlayer;

/**
 * @author mate
 * This is the format of the Json messages for the GoldmannGame
 */
public class GGMEssage {

    private int password;
    private String name;
    private CharacterNameEnum characterType;
    private Position playerPosition;
    private String time;
    private int lifeState;
    private StateOfPlayer stateOfPlayer;
    private GameStates gameState;
    private String feedbackMsg;

    public GGMEssage() {

    }

    /**
     * This datas will be send over the network.
     *
     * @param password       is the password for the game
     * @param name           is the name of the other player
     * @param characterType  is the type of other players character
     * @param playerPosition position of the other player
     * @param time           is the common time
     * @param lifeState      is the other players lifestate
     * @param stateOfPlayer  is the state of the other player
     * @param gameState      is the common gamestate
     */
    public GGMEssage(int password, String name, CharacterNameEnum characterType, Position playerPosition, String time,
                     int lifeState, StateOfPlayer stateOfPlayer, GameStates gameState) {
        this.password = password;
        this.name = name;
        this.characterType = characterType;
        this.playerPosition = playerPosition;
        this.time = time;
        this.lifeState = lifeState;
        this.stateOfPlayer = stateOfPlayer;
        this.gameState = gameState;
        this.feedbackMsg = null;
    }

    /**
     * @return password of the message
     */
    public int getPassword() {
        return password;
    }

    /**
     * set the password for the message
     *
     * @param password
     */
    public void setPassword(int password) {
        this.password = password;
    }

    /**
     * @return the name of the other player
     */
    public String getName() {
        return name;
    }

    /**
     * set name of the own player
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return charactertype of the other player
     */
    public CharacterNameEnum getCharacterType() {
        return characterType;
    }

    /**
     * set charactertype of the own player
     *
     * @param characterType
     */
    public void setCharacterType(CharacterNameEnum characterType) {
        this.characterType = characterType;
    }

    /**
     * @return position of the other player
     */
    public Position getPlayerPosition() {
        return playerPosition;
    }

    /**
     * set position of the own character
     *
     * @param playerPosition
     */
    public void setPlayerPosition(Position playerPosition) {
        this.playerPosition = playerPosition;
    }

    /**
     * @return the time as a String object
     */
    public String getTime() {
        return time;
    }

    /**
     * set the time for the common timer
     *
     * @param time
     */
    public void setTime(String time) {
        this.time = time;
    }

    /**
     * @return the lifestate of the other player
     */
    public int getLifeState() {
        return lifeState;
    }

    /**
     * set lifestate of the own player
     *
     * @param lifeState
     */
    public void setLifeState(int lifeState) {
        this.lifeState = lifeState;
    }

    /**
     * @return the state of other players character (STANDING, STEPl etc.)
     */
    public StateOfPlayer getStateOfPlayer() {
        return stateOfPlayer;
    }

    /**
     * setst the state of the own character
     *
     * @param stateOfPlayer
     */
    public void setStateOfPlayer(StateOfPlayer stateOfPlayer) {
        this.stateOfPlayer = stateOfPlayer;
    }

    /**
     * @return the gamestate (ENDED, CONNECTED etc)
     */
    public GameStates getGameState() {
        return gameState;
    }

    /**
     * @param gameState
     */
    public void setGameState(GameStates gameState) {
        this.gameState = gameState;
    }

    /**
     *
     * @return the feedback message, which is to send
     */
    public String getFeedbackMsg() {
        return feedbackMsg;
    }

    /**
     *
     * @param feedbackMsg is a string to send for the enemy
     */
    public void setFeedbackMsg(String feedbackMsg) {
        this.feedbackMsg = feedbackMsg;
    }

    /**
     * @return
     */
    @Override
    public String toString() {
        String data;

        data = "password:" + password + "\nname:\"" + name + "\"\nchacaterType:\"" + characterType.toString() +
                "\nplayerPosition:" + playerPosition + "\nlifestate:" + lifeState + "\nstateOfPlayer:" + stateOfPlayer +
                "\ntime:\"" + time +
                "\"\ngameState:\"" + gameState.toString() + "\nfeedback message:" + feedbackMsg + "\"";
        return data;
    }
}
