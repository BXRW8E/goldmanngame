package goldmann.GUI;

import javafx.scene.canvas.GraphicsContext;
import goldmann.tools.Jump_Tool;
import goldmann.tools.StateOfPlayer;

public class Player {
    private String userName;
    private Character character;
    private int lifeState;
    private Jump_Tool jump_tool;
    private double pos;

    /**
     * @param userName:  nameField of the player
     * @param character: type of the characterCombo
     */
    public Player(String userName, Character character) {
        this.userName = userName;
        this.character = character;
        lifeState = 100;
        jump_tool = new Jump_Tool(true, 0.0);
        pos = 0.0;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Position getCharacterPosition() {
        return character.getCharacterPosition();
    }


    public Character getCharacter() {
        return character;
    }

    public int getLifeState() {
        return lifeState;
    }

    public void setLifeState(int lifeState) {
        if (lifeState < 0) {
            this.lifeState = 0;
        } else if (lifeState > 100) {
            this.lifeState = 100;
        } else
            this.lifeState = lifeState;
    }

    /**
     * Lifestate will be decremented by the streght of a hit
     * if Player dies, dead will be set to true, else method returns false
     *
     * @param hitStrength
     * @return
     */
    public boolean decrementLifeState(int hitStrength) {
        boolean dead = false;
        setLifeState(this.lifeState - hitStrength);
        if (getLifeState() == 0) {
            dead = true;
            character.setState(StateOfPlayer.LOSE_L, 0.0);
        }
        return dead;
    }

    public void render(GraphicsContext gc) {
        if(gc != null && character.getCharacterPosition() != null && character.getActiveImage() != null) {
            gc.drawImage(character.getActiveImage(), getCharacterPosition().getX(), getCharacterPosition().getY());
        }
        //character.showHitBox(gc);
        //character.showBoundary(gc);
    }

    public void update(double time) {
        character.update(time);
    }

    public void setState(StateOfPlayer state, double t) {
        character.setState(state, t);
    }

    public boolean direction() {
        return character.getDirection();
    }

    public void jump(double t) {

        if (jump_tool.isJump_enabled()) {
            pos = getCharacterPosition().getY();
            jump_tool.setTimeup(t + 0.3);
            jump_tool.setJump_enabled(false);
        }
    }

    public boolean jumpEnabled() {
        return jump_tool.isJump_enabled();
    }

    public void back(double t) {
        if (jump_tool.isJump_enabled())
            return;

        if (jump_tool.isJumpUp(t)) {
            character.getVelocity().addVelocity(0, -800);
        } else if ((pos - getCharacterPosition().getY()) > 0.01) {
            character.getVelocity().addVelocity(0, +800);
        } else {
            getCharacterPosition().setY(pos);
            jump_tool.setJump_enabled(true);
        }
    }

    public void getHitted(Character c) { // returns if dead
        if (character.getHitted(c)) {
            if (decrementLifeState(5)) {
                c.setActiveImage(StateOfPlayer.WIN_L, 0.0);
            }
            System.out.println(userName + "hitted:" + lifeState);
        }
    }
}
