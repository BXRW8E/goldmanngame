package goldmann.GUI;

import goldmann.algorithm.LongValue;
import goldmann.networking.Network;
import goldmann.networking.NetworkStates;
import goldmann.networking.TCPClient;
import goldmann.networking.TCPServer;
import goldmann.tools.CharacterNameEnum;
import goldmann.tools.GameStates;
import goldmann.tools.StateOfPlayer;
import goldmann.tools.json.GGMEssage;
import goldmann.tools.json.JsonParserGG;
import javafx.animation.AnimationTimer;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;

public class AppLoop extends AnimationTimer {
    /*//** this is the message******************/
    private static int password;
    public Player yourPlayer;
    public Player enemyPlayer;
    private long startNanoTime;
    private LongValue lastNanoTime;
    private ArrayList<String> input;
    private double elapsedTime;
    private ViewGame viewGame;
    private ControllerStart controllerStart;
    private Network network;
    private Stage stage;
    private GameStates gameState;
    private GameStates prevState;
    private GameStates yourState;
    private GameStates enemyState;
    private String feedback;
    private int cntr = 0;       // with the counter in every 3. frame is the messagesending
    private double doubleTime;
    private CharacterNameEnum enemyCharacter;
    private boolean isFirst;
    private String name;
    private CharacterNameEnum characterType;
    private String time;
    private GGMEssage sendmsg;          /* message to send*/
    private GGMEssage ggmEssage;        /* message to receive */
    private boolean isFirstBroken;
    /*//****************************************/

    public AppLoop(long startNanoTime, LongValue lastNanoTime, Stage stage) {
        super();
        this.startNanoTime = startNanoTime;
        this.lastNanoTime = lastNanoTime;
        this.stage = stage;
        this.gameState = GameStates.STARTING;           // first state -- init
        this.prevState = GameStates.NONE;
        this.yourState = GameStates.NONE;
        this.enemyState = GameStates.NONE;
        doubleTime = 120.0;
        feedback = "";
        isFirst = true;                                 // marks the first run
        isFirstBroken = true;
    }

    public static int getPassword() {
        return password;
    }

    public void initplayers(boolean server) {

        if (server) {
            yourPlayer = new Player(controllerStart.getUserName(), new Character(controllerStart.getCaharacterType(), new Position(100, 250, true)));
            enemyPlayer = new Player("", new Character(enemyCharacter, new Position(900, 250, false)));
        } else {
            yourPlayer = new Player(controllerStart.getUserName(), new Character(controllerStart.getCaharacterType(), new Position(900, 250, false)));
            enemyPlayer = new Player("", new Character(enemyCharacter, new Position(100, 250, true)));
        }
    }

    /**
     * It is called if the <b>X</b> button is clicked
     * sends a message with state ENDED so the other player will be not get socket exceptions
     * If there was a connection then set the state to BROKENCONNECTION
     * and send it to the other player and close the socket.
     */
    @Override
    public void stop() {
        try {
            /*
             */
            if (network != null) {
                if (network.getState() == NetworkStates.CONNECTED) {

                    sendmsg.setGameState(GameStates.BROKENCONNECTION);
                    network.send(JsonParserGG.toJson(sendmsg));

                    Thread.sleep(500);          // wait a little bit

                    network.closeConnection();
                }
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e1) {
            //e1.printStackTrace(); // nem kell mert ugy is kilepünk
        }
        super.stop();

    }

    /**
     * function is called in every frames
     *
     * @param currentNanoTime
     */
    @Override
    public void handle(long currentNanoTime) {
        // calculate time since last update.
        elapsedTime = (currentNanoTime - lastNanoTime.value) / 1000000000.0;
        double t = (currentNanoTime - startNanoTime) / 1000000000.0;
        lastNanoTime.value = currentNanoTime;

        switch (gameState) {
            case INGAME:
                /* gameplay */
                yourState = GameStates.INGAME;
                enemyState = GameStates.INGAME;
                doubleTime -= elapsedTime;
                viewGame.clearPauseOrEoGDisplay();

                /*Logic for the moves*/
                if (viewGame.isHitEnable()) {
                    enemyPlayer.getHitted(yourPlayer.getCharacter());
                    viewGame.setHitEnable(false);
                }
                if (input.contains("LEFT") && input.contains("CONTROL")) {
                    yourPlayer.setState(StateOfPlayer.PUNCH_STEP_L, t);
                } else if (input.contains("LEFT")) {
                    yourPlayer.setState(StateOfPlayer.STEP_L, t);
                } else if (input.contains("RIGHT") && input.contains("CONTROL")) {
                    yourPlayer.setState(StateOfPlayer.PUNCH_STEP_R, t);
                } else if (input.contains("RIGHT")) {
                    yourPlayer.setState(StateOfPlayer.STEP_R, t);
                } else if (input.contains("CONTROL")) {
                    if (yourPlayer.direction()) {
                        yourPlayer.setState(StateOfPlayer.PUNCH_R, t);
                    } else {
                        yourPlayer.setState(StateOfPlayer.PUNCH_L, t);
                    }
                } else {
                    if (yourPlayer.direction()) {
                        yourPlayer.setState(StateOfPlayer.STANDING_R, t);
                    } else yourPlayer.setState(StateOfPlayer.STANDING_L, t);
                }

                if (input.contains("UP") && yourPlayer.jumpEnabled()) {
                    yourPlayer.jump(t);
                } else if (!yourPlayer.jumpEnabled()) {
                    yourPlayer.back(t);
                }

                /* Game logic*/
                if (viewGame.isPauseSelected()) { // if pause is pushed @Gabor
                    yourState = GameStates.PAUSED;
                    gameState = yourState;
                    viewGame.displayPaused(true);
                    try {
                        sendmsg.setStateOfPlayer(yourPlayer.getCharacter().getState());
                        sendmsg.setLifeState(enemyPlayer.getLifeState());
                        sendmsg.setPlayerPosition(yourPlayer.getCharacterPosition());
                        sendmsg.setGameState(yourState);
                        network.send(JsonParserGG.toJson(sendmsg));
                    } catch (IOException e) {
                        e.printStackTrace();
                        gameState = GameStates.BROKENCONNECTION;
                    }
                    break;
                }

                if ((yourPlayer.getLifeState() <= 0) || (enemyPlayer.getLifeState() <= 0)) {
                    yourState = GameStates.ENDED;
                    gameState = yourState;
                    try {
                        sendmsg.setStateOfPlayer(yourPlayer.getCharacter().getState());
                        sendmsg.setLifeState(enemyPlayer.getLifeState());
                        sendmsg.setPlayerPosition(yourPlayer.getCharacterPosition());
                        sendmsg.setGameState(yourState);
                        network.send(JsonParserGG.toJson(sendmsg));
                    } catch (IOException e) {
                        e.printStackTrace();
                        gameState = GameStates.BROKENCONNECTION;
                    }
                    update(doubleTime);
                    break;
                }
                cntr++;

                /* send message every in every 3 frames or in case of changes appear*/
                if (cntr > 3 || yourState != GameStates.INGAME) {
                    gameState = yourState; // gamestate is set to tmp (stays INGAME if was not modified)
                    try {
                        sendmsg.setStateOfPlayer(yourPlayer.getCharacter().getState());
                        sendmsg.setLifeState(enemyPlayer.getLifeState());
                        sendmsg.setPlayerPosition(yourPlayer.getCharacterPosition());
                        sendmsg.setGameState(yourState);
                        network.send(JsonParserGG.toJson(sendmsg));
                    } catch (IOException e) {
                        gameState = GameStates.BROKENCONNECTION;
                        e.printStackTrace();
                    }
                    cntr = 0;

                }
                // Receiving data in every frames
                enemyPlayer.getCharacter().setCharacterPosition(ggmEssage.getPlayerPosition());
                yourPlayer.setLifeState(ggmEssage.getLifeState());
                enemyPlayer.getCharacter().setenemyState(ggmEssage.getStateOfPlayer(), t);

                // System.out.println(ggmEssage.getGameState());
                enemyState = ggmEssage.getGameState();
                if (enemyState.equals(GameStates.BROKENCONNECTION)) {
                    yourState = GameStates.BROKENCONNECTION;
                }

                if (enemyState.equals(GameStates.PAUSED)) {
                    viewGame.displayPaused(false);
                    gameState = enemyState;
                }
                if (enemyState.equals(GameStates.ENDED)) {
                    gameState = enemyState;
                    sendmsg.setLifeState(0);
                }


                try {
                    update(doubleTime);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }

                break;
            case STARTING:
                /* first state, starting up the game, load graphics
                 * */

                ggmEssage = null;
                sendmsg = null;
                
                if (network != null) {
                    if (network.getState() == NetworkStates.CONNECTED) {
                        try {
                            network.closeConnection();
                            network = null;

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                isFirst = true;
                try {
                    startMenu();                        // loading the menu
                    controllerStart.setFeedbackMessage(feedback);
                    gameState = GameStates.STARTED;
                } catch (IOException e) {
                    gameState = GameStates.BROKENCONNECTION;
                    e.printStackTrace();

                }

                break;
            case STARTED:
                /* after loading the UI waiting for events*/
                break;
            case CONNECTING:
                /* if serverbutton was selected create one server or create one client client
                 * and start the connection @mate*/
                if (isFirst) {
                    if (controllerStart.serverButton.isSelected()) {
                        network = new TCPServer(4444);
                    } else if (controllerStart.clientButton.isSelected()) {
                        network = new TCPClient(controllerStart.ipAddressField.getText(), 4444);
                    }
                    network.startConnection();
                    System.out.println("Start connecting..");
                    isFirst = false;
                }

                if (network != null) {
                    /* if the server or client succesfully connected to the other
                     * switch state
                     */
                    if (network.getState() == NetworkStates.CONNECTED) {

                        gameState = GameStates.CONNECTED;
                        try {
                            Thread.sleep(10);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }

                break;
            case CONNECTED:

                password = Integer.parseInt(controllerStart.getPassword());   // get the password from the startmenu
                name = controllerStart.getUserName();                           // get the own username and put it to the msg
                characterType = controllerStart.getCaharacterType();
                time = "02:00";

                try {
                    sendmsg = new GGMEssage(password, name, characterType,
                            new Position(), time, 100, StateOfPlayer.STANDING_L, GameStates.CONNECTED);
                    network.send(JsonParserGG.toJson(sendmsg));    // send initial message


                    if (ggmEssage != null) {
                        gameState = ggmEssage.getGameState(); // set the own state
                    }

                    // TODO: ide jön a passwordcheck
                    // if password is not ok
                    if (controllerStart.isServer()) {
                        if (ggmEssage != null) {
                            if (ggmEssage.getPassword() != Integer.parseInt(controllerStart.getPassword())) {
                                sendmsg.setGameState(GameStates.WRONGPASSWORD);
                                network.send(JsonParserGG.toJson(sendmsg));
                                gameState = GameStates.WRONGPASSWORD;
                                Thread.sleep(10);
                            } else {  // if password is ok then start game
                                sendmsg.setGameState(GameStates.STARTINGAME);
                                network.send(JsonParserGG.toJson(sendmsg));
                                //   gameState = GameStates.STARTINGAME;
                            }
                        }
                    }

                } catch (Exception e) {
                    gameState = GameStates.BROKENCONNECTION;
                    e.printStackTrace();

                }

                /* ha átugorná véletlenül a Startingame állapotot */
                if (ggmEssage != null) {
                    GameStates recGamestate = ggmEssage.getGameState();
                    if (recGamestate == GameStates.INGAME || recGamestate == GameStates.STARTINGAME) {
                        gameState = GameStates.STARTINGAME;
                    }
                }

                break;
            case WRONGPASSWORD:

                try {
                    network.closeConnection();      // if password is wrong close the connection
                    network = null;                 // network must be null to create later a new

                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (controllerStart.isServer()) {
                    /* server goes to connecting state
                     * and start a server, wait for client
                     * */
                    controllerStart.connectButton.setSelected(true);
                    controllerStart.setConnect();

                } else {
                    /*client goes to started state
                     * enable input fields, and wait for connect button
                     * */
                    controllerStart.setFeedbackMessage("Wrong password");
                    controllerStart.connectButton.setSelected(false);
                    controllerStart.setConnect();
                    // gameState = GameStates.STARTED;
                }
                isFirst = true;

                break;
            case STARTINGAME:
                /* if the connection was succesful starting the game */
                doubleTime = 120;
                try {

                    gameMenu();
                    this.input = viewGame.getCommands();

                    enemyCharacter = ggmEssage.getCharacterType();
                    /* set the character if received the message */
                    if (enemyCharacter != null) {
                        initplayers(controllerStart.isServer());
                        characterType = controllerStart.getCaharacterType();
                        sendmsg.setLifeState(100);
                        sendmsg.setStateOfPlayer(enemyPlayer.getCharacter().getState());
                        sendmsg.setGameState(GameStates.INGAME);                /* set if the players can be setted */
                    }


                    enemyState = ggmEssage.getGameState();
                    if (enemyState != GameStates.WAITFORRESTART) {
                        gameState = enemyState;
                    }
                    /*if(enemyState == GameStates.ENDED){
                        gameState = GameStates.STARTINGAME;
                        sendmsg.setGameState(GameStates.STARTINGAME);
                    }*/
                    sendmsg.setCharacterType(characterType);

                    network.send(JsonParserGG.toJson(sendmsg));

                    enemyPlayer.setUserName(ggmEssage.getName());
                    enemyPlayer.getCharacter().setName(ggmEssage.getCharacterType());       // sets the enemy players charactertype

                } catch (Exception e) {
                    gameState = GameStates.BROKENCONNECTION;
                    e.printStackTrace();

                }

                break;
            case PAUSED:
                viewGame.setPauseSelected(false);

                // check restart pushed
                if (viewGame.isRestartSelected()) { // restart pushed in own window
                    viewGame.setRestartSelected(false);
                    if (!ggmEssage.getGameState().equals(GameStates.WAITFORRESTART)) {
                        gameState = GameStates.WAITFORRESTART;
                    } else { // that means other player was already waiting for our restart
                        gameState = GameStates.STARTINGAME;
                    }
                    sendmsg.setGameState(gameState);
                    try {
                        network.send(JsonParserGG.toJson(sendmsg));
                    } catch (IOException e) {
                        gameState = GameStates.BROKENCONNECTION;
                        e.printStackTrace();

                    }
                    break;
                }
                // check own exit push
                if (viewGame.isExitToMainSelected()) {
                    viewGame.setExitToMainSelected(false);
                    gameState = GameStates.STARTING;
                    sendmsg.setGameState(GameStates.STARTING);
                    sendmsg.setFeedbackMsg("Your enemy pushed exit!");
                    try {
                        network.send(JsonParserGG.toJson(sendmsg));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                }
                // check own continue push
                if (viewGame.isContinueSelected()) {
                    viewGame.setContinueSelected(false);
                    gameState = GameStates.RETURNINGAME;
                    sendmsg.setGameState(GameStates.RETURNINGAME);
                    try {
                        network.send(JsonParserGG.toJson(sendmsg));
                    } catch (IOException e) {
                        gameState = GameStates.BROKENCONNECTION;
                        e.printStackTrace();
                    }
                    break;
                }
                // Check enemy's exit or continue push
                feedback = ggmEssage.getFeedbackMsg(); // received feedback message
                enemyState = ggmEssage.getGameState();
                if (!enemyState.equals(GameStates.INGAME) && !enemyState.equals(GameStates.WAITFORRESTART)) {
                    gameState = enemyState;
                }
                break;
            case RETURNINGAME:
                gameState = GameStates.INGAME;
                break;
            case TIMEISUP: // @Gabor
                viewGame.displayEndOfTheGame(false, true);
                // check own exit push
                if (viewGame.isExitToMainSelected()) {
                    viewGame.setExitToMainSelected(false);

                    gameState = GameStates.STARTING;
                    sendmsg.setGameState(GameStates.STARTING);
                    sendmsg.setFeedbackMsg("Your enemy pushed exit!");
                    try {
                        network.send(JsonParserGG.toJson(sendmsg));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    try {
                        network.closeConnection();
                        network = null;
                        isFirst = true;
                        controllerStart.connectButton.setSelected(false);
                    } catch (IOException e) {
                        gameState = GameStates.BROKENCONNECTION;
                        e.printStackTrace();
                    }

                    break;
                }
                // Check enemy's exit push
                feedback = ggmEssage.getFeedbackMsg(); // received feedback message
                enemyState = ggmEssage.getGameState();
                if (enemyState.equals(GameStates.STARTING)) { // enemy pushed exit
                    gameState = enemyState;
                }
                // check restart pushed
                if (viewGame.isRestartSelected()) { // restart pushed in own window
                    viewGame.setRestartSelected(false);
                    if (!ggmEssage.getGameState().equals(GameStates.WAITFORRESTART)) {
                        gameState = GameStates.WAITFORRESTART;
                    } else { // that means other player was already waiting for our restart
                        gameState = GameStates.STARTINGAME;
                    }
                    sendmsg.setGameState(gameState);
                    try {
                        network.send(JsonParserGG.toJson(sendmsg));
                    } catch (IOException e) {
                        gameState = GameStates.BROKENCONNECTION;
                        e.printStackTrace();
                    }
                    break;
                }

                break;

            case WAITFORRESTART: // @Gabor
                viewGame.setRestartDisabled(true);
                viewGame.setRestartSelected(false);
                if (ggmEssage.getGameState().equals(GameStates.STARTINGAME)) {
                    gameState = GameStates.STARTINGAME;
                    break;
                }
                viewGame.setInstructionString("Wait for your enemy to restart, \nor exit to Main.");
                if (viewGame.isExitToMainSelected()) {
                    viewGame.setExitToMainSelected(false);
                    gameState = GameStates.STARTING;
                    sendmsg.setGameState(GameStates.STARTING);
                    sendmsg.setFeedbackMsg("Your enemy pushed exit!");
                    try {
                        network.send(JsonParserGG.toJson(sendmsg));
                    } catch (IOException e) {
                        e.printStackTrace();
                        gameState = GameStates.BROKENCONNECTION;
                    }
                    break;
                }
                feedback = ggmEssage.getFeedbackMsg(); // received feedback message
                enemyState = ggmEssage.getGameState();
                if (enemyState.equals(GameStates.STARTING)) { // enemy pushed exit
                    gameState = enemyState;
                } else if (enemyState.equals(GameStates.RETURNINGAME)) {

                    gameState = enemyState;
                }
                break;
            case ENDED:

                if (yourPlayer.getLifeState() <= 0) { // loser
                    viewGame.displayEndOfTheGame(false, false);
                    yourPlayer.setState(StateOfPlayer.LOSE_L, 0.0);
                    enemyPlayer.setState(StateOfPlayer.WIN_L, 0.0);
                } else { // winner
                    viewGame.displayEndOfTheGame(true, false);
                    yourPlayer.setState(StateOfPlayer.WIN_L, 0.0);
                    enemyPlayer.setState(StateOfPlayer.LOSE_L, 0.0);
                    enemyPlayer.setLifeState(0);
                }

                enemyState = ggmEssage.getGameState();
                /* Game logic*/
                if (viewGame.isRestartSelected()) { // restart pushed in own window
                    viewGame.setRestartSelected(false);
                    if (!enemyState.equals(GameStates.WAITFORRESTART)) {
                        gameState = GameStates.WAITFORRESTART;
                    } else { // that means other player was already waiting for our restart
                        gameState = GameStates.STARTINGAME;
                    }
                    sendmsg.setGameState(gameState);
                    try {
                        network.send(JsonParserGG.toJson(sendmsg));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    break;
                }
                update(doubleTime);
                // check own exit push
                if (viewGame.isExitToMainSelected()) {
                    viewGame.setExitToMainSelected(false);
                    gameState = GameStates.STARTING;
                    sendmsg.setGameState(GameStates.STARTING);
                    sendmsg.setFeedbackMsg("Your enemy pushed exit!");
                    try {
                        network.send(JsonParserGG.toJson(sendmsg));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                }
                if (enemyState.equals(GameStates.STARTING)) { // enemy pushed exit
                    gameState = enemyState;
                }

                break;
            case BROKENCONNECTION:
                /* if the connection is broken */
                if (isFirstBroken) {
                    try {
                        viewGame.displayEndOfTheGame(true, true);      // display message
                        if (network != null) {
                            network.closeConnection();      // if someone closes the game close the connection @mate
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                    isFirstBroken = false;
                }
                if (viewGame.isExitToMainSelected()) {
                    viewGame.setExitToMainSelected(false);
                    gameState = GameStates.STARTING;
                }
                break;

            default:
                break;
        }
        if (prevState != gameState)

        {
            /*print state just for debug */
            System.out.println(gameState);
        }

        prevState = gameState;
    }

    public void handleExitButton() {

    }

    public void handleRestartButton() {

    }

    public void update(double dtime) {
        yourPlayer.update(elapsedTime);
        //enemyPlayer.update(elapsedTime);
        String strTime = "00:00";
        if ((int) dtime > 0) {
            strTime = String.format("%02d:%02d", ((((int) dtime) / 60)), (((int) dtime) % 60));
        } else {
            strTime = "00:00";
            gameState = GameStates.TIMEISUP;
        }
        if (yourPlayer != null && enemyPlayer != null && controllerStart != null) {
            viewGame.rePaint(yourPlayer, enemyPlayer, strTime, controllerStart.isServer());
        }
    }

    /**
     * Visualize the game field
     *
     * @throws IOException
     */
    public void gameMenu() throws IOException {
        FXMLLoader fxmlLoader2 = new FXMLLoader(getClass().getResource("gameScene.fxml"));
        Parent rootGame = fxmlLoader2.load();
        this.viewGame = fxmlLoader2.<ViewGame>getController();       // get the controllerStart from fxml file

        Scene gameScene = new Scene(rootGame);
        stage.setScene(gameScene);
        //viewGame.setPassword(password);
        stage.show();
    }

    /**
     * Visualize the startmenu
     *
     * @throws IOException
     */
    public void startMenu() throws IOException {
        FXMLLoader fxmlLoader1 = new FXMLLoader(getClass().getResource("startPage.fxml"));
        Parent root = fxmlLoader1.load();
        this.controllerStart = fxmlLoader1.<ControllerStart>getController();       // get the controllerStart from fxml file

        controllerStart.setAppLoop(this);
        stage.setTitle("Goldmann Game");
        stage.setResizable(false);
        Scene theScene = new Scene(root);
        stage.setScene(theScene);

        stage.show();    // show window
    }

    public GGMEssage getGgmEssages() {
        return ggmEssage;
    }

    public void setGgmEssages(GGMEssage ggmEssage) {
        this.ggmEssage = ggmEssage;
    }

    public ControllerStart getControllerStart() {
        return controllerStart;
    }

    public GameStates getGameState() {
        return gameState;
    }

    public void setGameState(GameStates gameState) {
        this.gameState = gameState;
    }
}